require 'json'
require 'aws-sdk-cloudwatch'
require 'aws-sdk-dynamodb'

module Function
  class Handler
    @dynamodb = Aws::DynamoDB::Client.new
    @cloudwatch = Aws::CloudWatch::Client.new

    def self.process(event:,context:)
      last_evaluated_key = nil
      loop do
        results = @dynamodb.scan({
          table_name: ENV['SubdomainsSSLTableName'],
          exclusive_start_key: last_evaluated_key,
          expression_attribute_names: {
            "#Subdomains" => "Subdomains", 
            "#expiresEpochTime" => "expiresEpochTime", 
          }, 
          projection_expression: "#Subdomains, #expiresEpochTime", 
        })
        last_evaluated_key = results.last_evaluated_key

        results.items.each do |item|
          expiresIn = ((item['expiresEpochTime'] - Time.now.to_i) / 60 / 60 / 24).to_f
          puts "#{item['Subdomains']} expires in #{expiresIn.to_s}"

          @cloudwatch.put_metric_data({
            namespace: "CertExpiration",
            metric_data: [
              {
                metric_name: "ExpiresInInDays",
                dimensions: [
                  {
                    name: "Domain",
                    value: item['Subdomains'],
                  },
                ],
                timestamp: Time.now,
                values: [expiresIn],
                unit: "None"
              },
            ],
          })
        end

        break if last_evaluated_key.nil?

        sleep 1
      end
    end
  end
end
